#include "set.h"
#include "../common/Common.h"

struct Node {
   Hechizo elem; // el elemento que este nodo almacena
   Node* next; // siguiente nodo de la cadena de punteros
};

struct SetSt {
   int size; // cantidad de elementos del conjunto
   Node* first; // puntero al primer elemento
};

/**
  Invariantes de representacion:
    - size es la cantidad de nodos
    - no hay nodos con hechizos repetidos
**/

/// Proposito: retorna un conjunto de hechizos vacio
/// Costo: O(1)
Set emptyS() {
   Set setvacio= new SetSt;
   setvacio->size=0;
   setvacio->first=NULL;
   return setvacio;
}

/// Proposito: retorna la cantidad de hechizos
/// Costo: O(1)
int sizeS(Set s) {
    return s->size;
 }

/// Proposito: indica si el hechizo pertenece al conjunto
/// Costo: O(h), h = cantidad de hechizos
bool belongsS(Hechizo h, Set s) {
                Node* nodo= s->first;
                while((nodo!=NULL)&&(!mismoHechizo(nodo->elem,h))){
                nodo = nodo->next;
                }
                 return (nodo!=NULL);
}


/// Proposito: agrega un hechizo al conjunto
/// Costo: O(h), h = cantidad de hechizos
void addS(Hechizo h, Set s) {
    Node* nodo = new Node;
    nodo->elem= h;
    nodo->next= NULL;

    if(s->first == NULL){
           s->first = nodo;
            s->size++;

        }

    if (!belongsS (h,s)){
          Node* tmp = s->first;
          s->first = nodo;
          nodo->next = tmp;
          s->size++;
        }else{
        s=s;
        }





}

//PREGUNTAR
/// Proposito: borra un hechizo del conjunto (si no existe no hace nada)
/// Costo: O(h), h = cantidad de hechizos
void removeS(Hechizo h, Set s){
     if (belongsS(h,s)){
            Node* copia= s->first;
            Node* anteriores= NULL;
       if (s->size==1 && copia->elem==h){
			s->first=NULL;
       }
       while(copia!=NULL && copia->elem!=h){
		anteriores= copia;
		copia=copia->next;
       }
       if(anteriores==NULL){
		Node* tmp =copia;
		copia=tmp->next;
		delete tmp;
		s->size--;
       }else{
       Node* tmp= copia;
       anteriores->next=tmp->next;
       delete tmp;
       s->size--;
       }
     }
}

/// Proposito: borra toda la memoria consumida por el conjunto (pero no la de los hechizos)
/// Costo: O(n)
void destroyS(Set s){
    Node* borrando = s->first;
    Node* aux = borrando;
    while (borrando!=NULL){
        aux=aux->next;
        delete borrando;
        borrando=aux;
    }
    delete s;
}

/// Proposito: retorna un nuevo conjunto que es la union entre ambos (no modifica estos conjuntos)
/// Costo: O(h^2), h = cantidad de hechizos
Set unionS(Set s1,Set s2){
   Set nuevo = emptyS();
   Node* n1 = s1->first;
   Node* n2 = s2->first;
        while(n1 != NULL){
            addS(n1->elem,nuevo);
            n1 = n1->next;
        }

        while(n2 != NULL){
            addS(n2->elem,nuevo);
            n2 = n2->next;
        }

        return nuevo;
   }
