#include <iostream>
#include <cstdlib>
#include <vector>
#include "../hechizo/hechizo.h"
#include "../mago/mago.h"
#include "../set/set.h"
#include "../map/map.h"
#include "../maxheap/maxheap.h"
#include "escuela_magia.h"
#include "../common/Common.h"

using namespace std;

/// Proposito: Retorna todos los hechizos aprendidos por los magos.
/// Eficiencia:O(1)+O(h^2)+O(log m)
Set hechizosAprendidos(EscuelaDeMagia m) {
    Set nuevo= emptyS();
      for(string nombre:magos(m)){
      nuevo= unionS(hechizosDe(nombre,m),nuevo);
      }
      return nuevo;
}

/// Proposito: Indica si existe un mago que sabe todos los hechizos enseñados por la escuela.
/// Eficiencia: O(log m)
bool hayUnExperto(EscuelaDeMagia m) {
   return (leFaltanAprender(nombreMago(unEgresado(m)),m) == 0);
}

/// Proposito: Devuelve una maxheap con los magos que saben todos los hechizos dados por la escuela, quitándolos de la escuela.
/// Eficiencia: O(1) + O(log h^2)
MaxHeap egresarExpertos(EscuelaDeMagia m) {
     MaxHeap nuevo= emptyH();
      while(hayUnExperto(m)){
      insertH(unEgresado(m),nuevo);
      //agregar experto a una heap nueva preguntar como hacer,
      quitarEgresado(m);
      }
      return nuevo;
}

void nombresMagos(EscuelaDeMagia m){
        vector<string> ms = magos(m);
        for ( string s : ms) {
           cout << s << endl;
        }
}

int main()
{


        Hechizo h1= crearHechizo("h1",25);
        Hechizo h2= crearHechizo("h2",63);
        Hechizo h3= crearHechizo("h3",58);
        Hechizo h4= crearHechizo("h4",528);
        Hechizo h5= crearHechizo("h5",151);
        Hechizo h6= crearHechizo("h6",255);
        Hechizo h7= crearHechizo("h7",23);
        Hechizo h8= crearHechizo("h8",455);

        EscuelaDeMagia elcole= fundarEscuela();

        registrar("peter",elcole);
        registrar("elsa",elcole);
        registrar("roque",elcole);
        registrar("john", elcole);
        registrar("jaimito", elcole);
        registrar("pepito",elcole);
        registrar("miranda",elcole);
        registrar("tobias",elcole);

        enseniar(h1,"jaimito",elcole);
        enseniar(h6,"jaimito",elcole);
        enseniar(h5,"jaimito",elcole);
        enseniar(h2,"jaimito",elcole);
        enseniar(h2,"peter",elcole);
        enseniar(h3,"elsa",elcole);
        enseniar(h4,"jaimito",elcole);
        enseniar(h3,"peter",elcole);
        enseniar(h3,"jaimito",elcole);
        enseniar(h7,"jaimito",elcole);
        enseniar(h8,"jaimito",elcole);
        enseniar(h6,"roque",elcole);
        enseniar(h1,"john",elcole);
        enseniar(h2,"john",elcole);
        enseniar(h7,"pepito",elcole);
        enseniar(h7,"miranda",elcole);
        enseniar(h7,"tobias",elcole);
        enseniar(h8,"pepito",elcole);
        enseniar(h1,"miranda",elcole);
        enseniar(h2,"miranda",elcole);
        enseniar(h3,"miranda",elcole);
        enseniar(h4,"miranda",elcole);
        enseniar(h8,"miranda",elcole);
        enseniar(h6,"miranda",elcole);
        enseniar(h5,"miranda",elcole);


         cout<<leFaltanAprender(nombreMago(unEgresado(elcole)),elcole)<<endl;

        cout<<magos(elcole).size()<<endl;
        quitarEgresado(elcole);
       cout<<magos(elcole).size()<<endl;
        cout<<sizeS(hechizosAprendidos(elcole))<<endl;

        cout<<"el colegio esta vacio? " <<estaVacia(elcole)<<endl;
         cout<< "la lista de magos del colegio son: "<< endl;

         cout<<sizeS(hechizosAprendidos(elcole))<<endl;
         cout<<"para saber la cantidad de hechizos aprendidos por los magos presione entre 1 y 8 "<<endl;
         nombresMagos(elcole);
         cout<<"para seguir con las demas pruebas presione una tecla que no este entre 1 y 8"<<endl;

        int n = 0;
        cin >> n;

        switch(n){
         case 1: cout<< "la cantidad de hechizos de john son: "<<sizeS(hechizosDe("john",elcole));

         break;

         case 2:cout<< "la cantidad de hechizos de jaimito son: "<<sizeS(hechizosDe("jaimito",elcole));
         break;

         case 3:cout<< "la cantidad de hechizos de elsa son: "<<sizeS(hechizosDe("elsa",elcole));
         break;


         case 4: cout<< "la cantidad de hechizos de peter son: "<<sizeS(hechizosDe("peter",elcole));
         break;

         case 5: cout<< "la cantidad de hechizos de pepito son: "<<sizeS(hechizosDe("pepito",elcole));
         break;

         case 6: cout<< "la cantidad de hechizos de miranda son: "<<sizeS(hechizosDe("miranda",elcole));
         break;

         case 7: cout<< "la cantidad de hechizos de roque son: "<<sizeS(hechizosDe("roque",elcole));
         break;

         case 8: cout<< "la cantidad de hechizos de tobias son: "<<sizeS(hechizosDe("tobias",elcole));
         break;

         default:
         cout<< "el primer egresado seria " << nombreMago(unEgresado(elcole))<<endl;
		 cout<< "la cantidad de magos son: " << magos(elcole).size()<<endl;
		 cout<< "el mago numero 2 es: "<< (magos(elcole)[2])<<endl;
         cout<< "la cantidad de hechizos del colegio son: "<< sizeS(hechizosAprendidos(elcole))<<endl;
         cout <<"al mago de indice 2 le faltan aprender "<< leFaltanAprender(magos(elcole)[2],elcole)<< " hechizos"<<endl;
         cout <<"al mago de indice 1 le faltan aprender "<< leFaltanAprender(magos(elcole)[1],elcole)<<" hechizos"<<endl;
         cout <<"al mago de indice 3 le faltan aprender "<< leFaltanAprender(magos(elcole)[3],elcole)<<" hechizos"<<endl;
         cout <<"al mago de indice 4 le faltan aprender "<< leFaltanAprender(magos(elcole)[4],elcole)<<" hechizos"<<endl;

         cout<<"hay un experto? " << hayUnExperto(elcole)<<endl;
         cout<<"la cantidad de expertos son: "<<sizeH(egresarExpertos(elcole))<<endl;
         cout<< " quitamos un egresado"<<endl;

         cout<<"hay un experto? " <<hayUnExperto(elcole)<<endl;
		 cout<<"la cantidad de magos ahora es: "<< magos(elcole).size()<<endl;

		 }
     return 0;

}

