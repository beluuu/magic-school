#include "escuela_magia.h"
#include "../common/Common.h"

struct EscuelaDeMagiaSt {
    Set hechizos;
    Map magosM;
    MaxHeap magosH;
};

/// Prop�sito: Devuelve una escuela vac�a.
/// O(1)
EscuelaDeMagia fundarEscuela() {
    EscuelaDeMagiaSt* escuelaDeMagia=new EscuelaDeMagiaSt;
            escuelaDeMagia->hechizos=emptyS();
            escuelaDeMagia->magosM=emptyM();
            escuelaDeMagia->magosH=emptyH();
            return escuelaDeMagia;
}

/// Prop�sito: Indica si la escuela est� vac�a.
/// O(1)
bool estaVacia(EscuelaDeMagia m) {
    return (isEmptyH(m->magosH));
}

/// Prop�sito: Incorpora un mago a la escuela (si ya existe no hace nada).
/// O(log m)
void registrar(string nombre, EscuelaDeMagia m){
            if(lookupM(nombre,m->magosM)== NULL){
                assocM(nombre,crearMago(nombre),m->magosM);
                insertH(crearMago(nombre),m->magosH);

            }

}

/// Prop�sito: Devuelve los nombres de los magos registrados en la escuela.
/// O(m)
vector<string> magos(EscuelaDeMagia m) {
        return domM(m->magosM);
}

/// Prop�sito: Devuelve los hechizos que conoce un mago dado.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m)
Set hechizosDe(string nombre, EscuelaDeMagia m) {
            return hechizosMago(lookupM(nombre,m->magosM));
}

/// Prop�sito: Dado un mago, indica la cantidad de hechizos que la escuela ha dado y �l no sabe.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m)
int leFaltanAprender(string nombre, EscuelaDeMagia m) {
       return sizeS(m->hechizos) - sizeS(hechizosDe(nombre,m));
}

/// Prop�sito: Devuelve el mago que m�s hechizos sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m)
Mago unEgresado(EscuelaDeMagia m) {
      return maxH(m->magosH);
}

/// Prop�sito: Devuelve la escuela sin el mago que m�s sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m)
void quitarEgresado(EscuelaDeMagia m) {
      deleteM(nombreMago(unEgresado(m)),m->magosM);
      deleteMax(m->magosH);
}
MaxHeap actualizando(Hechizo hechizo,string nombre,MaxHeap h){
            MaxHeapSt* nuevo= emptyH();
                while(sizeH(h)!=0) {
                   if(nombreMago(maxH(h))!=nombre){
                   insertH(maxH(h),nuevo);
                   deleteMax(h);
                   }else{
                   aprenderHechizo(hechizo,maxH(h));
                   insertH(maxH(h),nuevo);
                   deleteMax(h);
    }
  }
    return nuevo;
}

/// Prop�sito: Ense�a un hechizo a un mago existente, y si el hechizo no existe en la escuela es incorporado a la misma.
/// O(m . log m + log h)
void enseniar(Hechizo h, string nombre, EscuelaDeMagia m) {
                addS(h,m->hechizos);
                aprenderHechizo(h,lookupM(nombre,m->magosM));
                m->magosH=actualizando(h,nombre,m->magosH);

}

/// Prop�sito: Libera toda la memoria creada por la escuela (incluye magos, pero no hechizos).
void destruirEscuela(EscuelaDeMagia m) {
        destroyS(m->hechizos);
        destroyM(m->magosM);
       while(m->magosH!=NULL){
			destroyMago(maxH(m->magosH));
			deleteMax(m->magosH);
        }
		destroyH(m->magosH);
}
